extends KinematicBody2D

var motion = Vector2()
var gravity = 2

func _ready():
	set_process(true)

func _process(delta):
	motion.y += gravity * delta
	move_and_collide(motion)
